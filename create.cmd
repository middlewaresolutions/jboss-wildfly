set PROJECT=jboss-apps

oc login -u developer -p developer
oc new-project %PROJECT%

oc login -u system:admin
oc project %PROJECT%
oc adm policy add-scc-to-user anyuid -z default

oc login -u developer -p developer

oc new-build . --name=jboss-wildfly --strategy=docker

oc expose svc/jboss-wildfly

