input {
  file {
    path => "${WILDFLY_HOME}/standalone/log/access_log*"
    start_position => "beginning"
  }
}

filter {
    
    grok {
      
        # %t %I %h %l %u %v %m %U %q '%{i,Referer}' %s %b %D
        # %t - Date and time, in Common Log Format
        # %I - Current request thread name (can compare later with stacktraces)
        # %h - Remote host name (or IP address if enableLookups for the connector is false)
        # %l - Remote logical username from identd (always returns '-')
        # %u - Remote user that was authenticated (if any), else '-'
        # %v - Local server name
        # %m - Request method (GET, POST, etc.)
        # %U - Requested URL path
        # %q - Query string (prepended with a '?' if it exists)
        # %s - HTTP status code of the response
        # '%{i,Referer}' - Origin
        # %s - HTTP status code of the response
        # %b - Bytes sent, excluding HTTP headers, or '-' if zero
        # %D - Time taken to process the request, in millis
      match => { "message" => "%{HTTPDATE:logdate}] %{DATA:worker} %{DATA:thread} %{IP:clientip} %{DATA:username} %{DATA:login} %{DATA:server} %{WORD:method} %{URIPATH:uri} %{DATA:query} '%{DATA:referer}' %{NUMBER:status} %{NUMBER:bytes} %{DATA:delay}" }

# [25/Jul/2017:11:14:13 +0200] default task-5 127.0.0.1 - - localhost:8080 GET /openshift-tasks/ ?var=123 '-' 200 11515 -
# [25/Jul/2017:11:14:13 +0200] default task-7 127.0.0.1 - - localhost:8080 GET /ws/demo/healthcheck - 'http://localhost:8080/openshift-tasks/?var=123' 404 74 -
    }
  
    # set date in line
    date {
      match => [ "logdate" , "dd/MMM/yyyy:HH:mm:ss Z" ]
      remove_field => ["timestamp"]
    }

    # IP of client
    geoip {
      source => "clientip"
    }

    # convert types
    mutate {
      add_field => { 
        "type" => "http_access" 
        "application" => "${APPLICATION}" 
      }

      convert => {
        "status" => "integer"
        "bytes" => "integer"
        "delay" => "integer"
      }
    }

}

output {
  elasticsearch {
    hosts => ["${ELASTICSEARCH_HOST}"]
    user => "${ELASTICSEARCH_USER}"
    password => "${ELASTICSEARCH_PWD}"
    index => "access-%{+YYYY.MM.dd}"
  }
  stdout { codec => rubydebug }
}