#!/bin/sh
# add parameters with -E NAME=VALUE

# Start MetricBeat
METRICBEAT_HOME=/usr/share/metricbeat/

# start process
nohup $METRICBEAT_HOME/metricbeat -v -c $METRICBEAT_HOME/metricbeat.yml $@ &

# wait 2 sec
sleep 2

# trace first trace
cat $METRICBEAT_HOME/logs/metricbeat
