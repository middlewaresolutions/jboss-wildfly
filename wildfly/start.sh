#!/bin/sh

# Start MetricBeat
$METRICBEAT_HOME/metricbeat.sh -E ES_HOST=$ELASTICSEARCH_HOST -E ES_USER=$ELASTICSEARCH_USER -E ES_PWD=$ELASTICSEARCH_PWD -E APPLICATION=$APPLICATION_NAME -E ENV=$ENVIRONMENT -E VERSION=$APPLICATION_VERSION

# Start Logstash
$LOGSTASH_HOME/bin/logstash -f jboss.conf

# Start Wildfly
$WILDFLY_HOME/bin/standalone.sh -c standalone-full.xml -b 0.0.0.0 $@